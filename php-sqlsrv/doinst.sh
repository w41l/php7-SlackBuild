config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD|md5sum)" = "$(cat $NEW|md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

config etc/php.d/sqlsrv.ini.new
config etc/php.d/pdo_sqlsrv.ini.new

chroot . /usr/bin/pecl install --nodeps --soft --force --register-only --nobuild \
  usr/lib@LIBDIRSUFFIX@/php/.pkgxml/sqlsrv.xml > /dev/null

chroot . /usr/bin/pecl install --nodeps --soft --force --register-only --nobuild \
  usr/lib@LIBDIRSUFFIX@/php/.pkgxml/pdo_sqlsrv.xml > /dev/null
