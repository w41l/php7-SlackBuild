#!/bin/sh

# Slackware build script for "php-xmlrpc".

# Copyright 2021-2024  Widya Walesa <pkgs.w41l@walecha.web.id>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

SRCNAM=pecl-networking-xmlrpc
VERSION=${VERSION:-"1.0.0_7dade03"}
SRCVERSION=7dade03efe2aa11caee444968263e9f9bc8b2ea0
PHPVER=$(php-config --version | cut -d . -f 1,2 | tr -d .)
PRGNAM=php${PHPVER}-xmlrpc
SBNAM=php-xmlrpc
BUILD=${BUILD:-1}
TAG=${TAG:-_SBo}
PKGTYPE=${PKGTYPE:-tlz}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i686 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PRGNAM-$VERSION-$ARCH-${BUILD}${TAG}.$PKGTYPE"
  exit 0
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i386" ]; then
  SLKCFLAGS="-O2 -march=i386 -mcpu=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=pentium4 -mtune=generic"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -march=x86-64 -mtune=generic -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $SRCNAM-$SRCVERSION package.xml
tar xvf $CWD/$SRCNAM-$SRCVERSION.tar.gz
cd $SRCNAM-$SRCVERSION
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 \
  -o -perm 511 \) -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 640 -o -perm 600 -o -perm 444 \
  -o -perm 440 -o -perm 400 \) -exec chmod 644 {} \;

phpize
CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --enable-static=no \
  --enable-opcache \
  --build=$ARCH-slackware-linux

make
make install-modules INSTALL_ROOT=$PKG

mkdir -p $PKG/etc/php.d/
install -m 644 -o root -g root $CWD/xmlrpc.ini $PKG/etc/php.d/xmlrpc.ini.new

mkdir -p $PKG/usr/lib$LIBDIRSUFFIX/php/.pkgxml
install -m 644 package.xml $PKG/usr/lib$LIBDIRSUFFIX/php/.pkgxml/xmlrpc.xml

find $PKG -print0 | xargs -0 file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION
cp -a CREDITS EXPERIMENTAL LICENSE $PKG/usr/doc/$PRGNAM-$VERSION
cat $CWD/$SBNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$SBNAM.SlackBuild

mkdir -p $PKG/install
sed "s/${SBNAM}:/${PRGNAM}:/g" $CWD/slack-desc >$PKG/install/slack-desc
sed \
  -e "s|@LIBDIRSUFFIX@|$LIBDIRSUFFIX|g" \
  -e "s|@SRCNAM@|xmlrpc|g" \
  $CWD/doinst.sh >$PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE
