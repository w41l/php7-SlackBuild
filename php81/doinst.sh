config() {
  NEW="$1"
  OLD="`dirname $NEW`/`basename $NEW .new`"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "`cat $OLD | md5sum`" = "`cat $NEW | md5sum`" ]; then # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}
# Keep same perms on rc.php-fpm.new:
if [ -e etc/rc.d/rc.php-fpm ]; then
  cp -a etc/rc.d/rc.php-fpm etc/rc.d/rc.php-fpm.new.incoming
  cat etc/rc.d/rc.php-fpm.new > etc/rc.d/rc.php-fpm.new.incoming
  mv etc/rc.d/rc.php-fpm.new.incoming etc/rc.d/rc.php-fpm.new
fi
config etc/rc.d/rc.php-fpm.new
cp -a @PHP_CONFIG_DIR@/php.ini-production @PHP_CONFIG_DIR@/php.ini.new
config @PHP_CONFIG_DIR@/php.ini.new
cp -a @PHP_CONFIG_DIR@/php-fpm.conf.default @PHP_CONFIG_DIR@/php-fpm.conf.new
config @PHP_CONFIG_DIR@/php-fpm.conf.new
cp -a @PHP_CONFIG_DIR@/php-fpm.d/www.conf.default @PHP_CONFIG_DIR@/php-fpm.d/www.conf.new
config @PHP_CONFIG_DIR@/php-fpm.d/www.conf.new
